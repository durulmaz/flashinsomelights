<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>

 
</header>
<main>
    
  <article>
      
  <?php require APPROOT . '/views/competition/compnavbar.php' ?>


        
        <h2 >Add a player</h2>
        <p id=donald>Please fill out this form to add a player</p>
        

        <form action="<?php echo URLROOT; ?>/players/insertingone" method="post">
          <div class="form-group">
            <label for="FirstName">First Name: <sup>*</sup></label>
            <input type="text" name="FirstName" class="form-control form-control-lg <?php echo (!empty($data['FirstName_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['FirstName']; ?>">
            <span class="invalid-feedback"><?php echo $data['FirstName_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="LastName">Last Name: <sup>*</sup></label>
            <input type="text" name="LastName" class="form-control form-control-lg <?php echo (!empty($data['LastName_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['LastName']; ?>">
            <span class="invalid-feedback"><?php echo $data['LastName_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Email">Email: <sup>*</sup></label>
            <input type="email" name="Email" class="form-control form-control-lg <?php echo (!empty($data['Email_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Email']; ?>">
            <span class="invalid-feedback"><?php echo $data['Email_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Address1">Address 1: <sup>*</sup></label>
            <input type="text" name="Address1" class="form-control form-control-lg <?php echo (!empty($data['Address1_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Address1']; ?>">
            <span class="invalid-feedback"><?php echo $data['Address1_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Address2">Address 2: <sup>*</sup></label>
            <input type="text" name="Address2" class="form-control form-control-lg <?php echo (!empty($data['Address2_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Address2']; ?>">
            <span class="invalid-feedback"><?php echo $data['Address2_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="PostalCode">Postal code: <sup>*</sup></label>
            <input type="text" name="PostalCode" class="form-control form-control-lg <?php echo (!empty($data['PostalCode_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['PostalCode']; ?>">
            <span class="invalid-feedback"><?php echo $data['PostalCode_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="City">City: <sup>*</sup></label>
            <input type="text" name="City" class="form-control form-control-lg <?php echo (!empty($data['City_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['City']; ?>">
            <span class="invalid-feedback"><?php echo $data['City_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Country">Country: <sup>*</sup></label>
            <input type="text" name="Country" class="form-control form-control-lg <?php echo (!empty($data['Country_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Country']; ?>">
            <span class="invalid-feedback"><?php echo $data['Country_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Phone">Phone: <sup>*</sup></label>
            <input type="text" name="Phone" class="form-control form-control-lg <?php echo (!empty($data['Phone_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Phone']; ?>">
            <span class="invalid-feedback"><?php echo $data['Phone_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Birthday">Birthday: <sup>*</sup></label>
            <input type="text" name="Birthday" class="form-control form-control-lg <?php echo (!empty($data['Birthday_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Birthday']; ?>">
            <span class="invalid-feedback"><?php echo $data['Birthday_err']; ?></span>
          </div>
         
          <div class="dropdown">
         <span>Add to team</span>
          <div class="dropdown-content">
          <p>test</p>
          </div>
          </div>
         
         <br>
         <br>
         <br>
    
   















          <div class="row">
            <div class="col">
              <input type="submit" value="Add Player" class="btn btn-success btn-block">
            </div>
          </div>
        
        </form>
   



        </div>





  
  </article>

  <nav>side nav
  
  
  </nav>

<aside>
<?php foreach($data['players'] as $player) : ?>  
  <div class="card card-body mb-4">
  <h4 class="card-title">  <?php echo $player->FirstName;?></h4>
  <?php echo $player->LastName;?>
  <p class="card-text"><?php echo $player->LastName; ?></p>
  <a href="<?php echo URLROOT; ?>/players/showplayer/<?php echo $player->Id; ?>" class="btn btn-dark">More</a>
  </div>
   <?php endforeach; ?>





</aside>


  

</main>
<footer>
footer
<?php require APPROOT . '/views/inc/footer.php'; ?>
</footer>


</body>
  
