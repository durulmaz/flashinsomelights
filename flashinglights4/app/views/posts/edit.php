<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>
  
  
</header>
<main>
    
  <article>
    


  
   <a href="<?php echo URLROOT; ?>/posts" class="btn btn-light">cancel</a>
      <div class="card card-body bg-light mt-5">
   
        <h2>Edit Post</h2>
        <p>Use this form to edit a post</p>
        <form action="<?php echo URLROOT; ?>/posts/edit/<?php echo $data['id']; ?>" method="post">
          
          <div class="form-group">
            <label for="title">Title: <sup>*</sup></label>
            <input type="text" name="title" class="form-control form-control-lg <?php echo (!empty($data['title_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['title']; ?>">
            <span class="invalid-feedback"><?php echo $data['title_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="body">Body: <sup>*</sup></label>
            <textarea name="body" name="password" class="form-control form-control-lg <?php echo (!empty($data
            ['body_err'])) ? 'is-invalid' : ''; ?>"><?php echo $data['body'];?></textarea>
            <span class="invalid-feedback"><?php echo $data['body_err']; ?></span>
          </div>
        <input type='submit' class="btn btn-success" value="Submit">
        </form>
    </div>










      


 








  
  </article>

  <nav>side nav</nav>

  <aside>aside</aside>

</main>



<footer>
  footer
</footer>

</body>
  
<?php require APPROOT . '/views/inc/footer.php'; ?>