<?php
class Users extends controller{
    public function __construct(){
        $this->userModel = $this->model('User');



    }

    public function register(){
        // Check of het om een POST actie gaat
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            //zo ja, dan wordt het volgende uitgevoerd.

// Sanitize POST data
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            // Init data
            $data =[
                'firstname' => trim($_POST['firstname']),
                'lastname' => trim($_POST['lastname']),
                'age' => trim($_POST['age']),
                'location' => trim($_POST['location']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'confirm_password' => trim($_POST['confirm_password']),
                'firstname_err' => '',
                'lastname_err' =>'',
                'age_err' =>'',
                'location_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''
            ];

// validate email 
if(empty($data['email'])){
        $data['email_err'] = 'Please enter email';
} else {
    // check email
    if($this->userModel->findUserByEmail($data['email'])){
        $data['email_err'] = 'There is already a user registered with this e-mail address.';
    }
}

//validate first Name 
if(empty($data['firstname'])){
    $data['firstname_err'] = 'Please enter first name';
}

//validate Last Name 
if(empty($data['lastname'])){
    $data['lastname_err'] = 'Please enter last name';
}
//validate Age 
if(empty($data['age'])){
    $data['age_err'] = 'Please enter age';
}

//validate LOcation 
if(empty($data['location'])){
    $data['location_err'] = 'Please enter location';
}

//validate Password
if(empty($data['password'])){
    $data['password_err'] = 'Please enter password';
}elseif(strlen($data['password']) < 6){
    $data['password_err'] = 'Password must be at least 6 characters';
}

//validate confirm Password
if(empty($data['confirm_password'])){
    $data['confirm_password_err'] = 'Please confirm password';
}else{
    if($data['password'] !=$data['confirm_password']){
        $data['confirm_password_err'] = 'Passwords do not match';
    }
}

//Make sure errors are empty
if(empty($data['email_err']) && empty($data['firstname_err']) && empty($data['lastname_err']) && empty($data['age_err']) && empty($data['location_err']) && empty($data['password_err']) && 
empty($data['confirm_password_err'])){
    //Validate
    

    // hash Pasword
    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

    // Register User
    if($this->userModel->register($data)){
        flash('register_success', 'You are regisered and can log in');
        redirect('users/login');
    } else {
        die('Something went wrong');
    }

} else {
    // load view with errors
    $this->view('users/register', $data);
}

        }else {
            // Init data
            $data =[
                'firstname' => '',
                'lastname' => '',
                'age' => '',
                'location' => '',
                'email' => '',
                'password' => '',
                'confirm_password' => '',
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''
            ];
            // load view
            $this->view('users/register', $data);
        }
    }

    public function login(){
        // Check for POST
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
          // Process form
          // Sanitize POST data
          $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
          
          // Init data
          $data =[
            'email' => trim($_POST['email']),
            'password' => trim($_POST['password']),
            'email_err' => '',
            'password_err' => '',      
          ];
  
          // Validate Email
          if(empty($data['email'])){
            $data['email_err'] = 'Pleae enter email';
          }
  
          // Validate Password
          if(empty($data['password'])){
            $data['password_err'] = 'Please enter password';
          }

          // Check for user/email
          if($this->userModel->findUserByEmail($data['email'])){
              // user found
          }else {
              //User not found
              $data['email_err'] = 'No user found with this email address.';
          }


  
          // Make sure errors are empty
          if(empty($data['email_err']) && empty($data['password_err'])){
            // Validated
            // Check and set logged in user
            $loggedInUser = $this->userModel->login($data['email'], $data['password']);

            if($loggedInUser){
                //Create Session
                $this->createUserSession($loggedInUser);

            } else{
                $data['password_err'] = 'Password incorrect';

                $this->view('users/login', $data);
            }

          } else {
            // Load view with errors
            $this->view('users/login', $data);
          }
  
  
        } else {
          // Init data
          $data =[    
            'email' => '',
            'password' => '',
            'email_err' => '',
            'password_err' => '',        
          ];
  
          // Load view
          $this->view('users/login', $data);
        }
      }


public function createUserSession($user){
$_SESSION['user_id'] = $user->id;
$_SESSION['user_email'] = $user->email;
$_SESSION['user_firstname'] = $user->firstname;
redirect('posts');
}


public function logout(){
    unset($_SESSION['user_id']);
    unset($_SESSION['user_email']);
    unset($_SESSION['user_firstname']);
    session_destroy();
    redirect('users/login');

}



    }