<?php
class Posts extends Controller{
public function __construct(){
    if(!isLoggedIn()){
        redirect('users/login');
    }
    $this->postModel = $this->model('Post');
    $this->userModel = $this->model('User');
    
}

// gebruiken als template om alle posts te tonen.
    public function index(){
        // Get posts
        $posts = $this->postModel->getPosts();
        $data = [
            'posts' => $posts];
        $this->view('posts/index', $data);
    }

    public function add(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // SANITIZE POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);


            $data = [
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'user_id' => $_SESSION['user_id'],
                'title_err' => '',
                'body_err' => ''
                        ];
                        // validate data
                        // validate title
                        if(empty($data['title'])){
                            $data['title_err'] = 'Please enter a title';
                        }
                        // validate body
                        if(empty($data['body'])){
                            $data['body_err'] = 'Please enter text';
                        }

                        // make sure there are no errors
                        if(empty($data['title_err']) && empty($data['body_err'])){
                            // validated
                            if($this->postModel->addPost($data)){
                                flash('post_message', 'Post Added');
                                redirect('posts');

                            }else{
                                die('Something went wrong');
                            }

                        }else {
                            // load the view with errors
                            $this->view('posts/add', $data);
                        }
    
        } else{
            $data = [
                'title' => '',
                'body' => '',
            ];
            $this->view('posts/add', $data);
        }
    }


    // hier wordt de waarde na posts/edit/1 gebruikt in de $id variable.. In dit geval de 1... dus het zou dan gaan om de post met id 1. 
    // dit kan dan opgevraagd worden in de database
    public function edit($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // SANITIZE POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);


            $data = [

                'id' => $id,
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'user_id' => $_SESSION['user_id'],
                'title_err' => '',
                'body_err' => ''
                        ];
                        // validate data
                        // validate title
                        if(empty($data['title'])){
                            $data['title_err'] = 'Please enter a title';
                        }
                        // validate body
                        if(empty($data['body'])){
                            $data['body_err'] = 'Please enter text';
                        }

                        // make sure there are no errors
                        if(empty($data['title_err']) && empty($data['body_err'])){
                            // validated
                            if($this->postModel->updatePost($data)){
                                flash('post_message', 'Post Updated');
                                redirect('posts');

                            }else{
                                die('Something went wrong');
                            }

                        }else {
                            // load the view with errors
                            $this->view('posts/edit', $data);
                        }
    
        } else{

            // get the existing post from the model
            $post = $this->postModel->getPostById($id);



            // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
            if($post->user_id != $_SESSION['user_id']){
                redirect('posts');
            }


            // hier gaan we de waardes meegeven aan onze view door de variable $data op te vullen
            $data = [
                'id' => $id,
                'title' =>  $post->title,
                'body' => $post->body
            ];

            // we nemen dus de id, title en body en geven die mee via $data aan de edit view
            // dit wordt dan samen aan de gebruiker getoond via de browser door de controler.
            $this->view('posts/edit', $data);
        }
    }


        // example : posts/show/3
        public function show($id){

            $post = $this->postModel->getPostById($id);
            $user = $this->userModel->getUserById($post->user_id);

            $data = [
                'post' => $post,
                'user' => $user

            ];

        $this->view('posts/show', $data);

        }

    public function delete($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // get the existing post from the model
            $post = $this->postModel->getPostById($id);
            // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
            if($post->user_id != $_SESSION['user_id']){
                redirect('posts');
            }
            if($this->postModel->deletePost($id)){
                flash('post_message', 'Post Removed');
                redirect('posts');
            }else {
                die('Something went wrong');
            }
        }else {
            redirect('posts');
        }

    }


      
    }








