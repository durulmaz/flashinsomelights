<?php
  class Games extends Controller {
    public function __construct(){
        if(!isLoggedIn()){
            redirect('users/login');
        }
       $this->gameModel = $this->model('Game');
       $this->userModel = $this->model('User');
    }
       
       public function game(){
        
        $game = $this->gameModel->getGames();
        $data = [
            'games' => $game
        ];
        $this->view('competition/game/game', $data);

    }



    public function insertingone(){
    
        // dit zal checken of het om een POST actie gaat 
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
    // het formulier uitvoeren
    
    //Sanitize post data 
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
    $data =[
        'Date' => trim($_POST['Date']),
        'Status' => trim($_POST['Status']),
        'ScoreHome' => trim($_POST['ScoreHome']),
        'ScoreVisitors' => trim($_POST['ScoreVisitors']),
        'Date_err' => '',
        'Status_err' =>'',
        'ScoreHome_err' => '',
        'ScoreVisitors_err' => ''
    ];
    
    // validate name
    if(empty($data['Date'])){
        $data['Date_err'] = 'Please enter a date';
    }
    // validate location
    if(empty($data['Status'])){
        $data['Status_err'] = 'Please enter a status';
    }
    // validate score
    if(empty($data['ScoreHome'])){
        $data['ScoreHome_err'] = 'Please enter a home score';
    }
    // validate score
    if(empty($data['ScoreVisitors'])){
        $data['ScoreVisitors_err'] = 'Please enter a visitors score';
    }

    
    // zorg dat alle error's leeg zijn
    if(empty($data['Date_err']) && empty($data['Status_err']) && empty($data['ScoreHome_err']) && empty($data['ScoreVisitors_err']))
    {
        // validated
       // die('SUCCESS');
    
    if($this->gameModel->insertingone($data)){
        flash('register_success', 'The game has been added');
        redirect('games/Game');
        
    } else{
        die('Something went wrong');
    }
    
    } else{
        // load view with errors
        $this->view('competition/game/insertingone', $data);
    
    }
    
    
     } else {
            // het formulier laden ( test hieronder of de url football/insertplayer werkt met de echo)
            //echo 'laadt de spelerslijst';
    //initialiseer data 
    $data =[
        'Date' =>  '',
        'Status' =>  '',
        'ScoreHome' =>  '',
        'ScoreVisitors' =>  '',
        'Date_err' => '',
        'Status_err' =>'',
        'ScoreHome_err' => '',
        'ScoreVisitors_err' => ''

    ];
    
    // load view
    $this->view('competition/game/insertingone', $data);

     }
    }




    public function showGame($id){

        $game = $this->gameModel->getGameById($id);
        
    
        $data = [
            'game' => $game,
            
    
        ];
    
    $this->view('competition/game/showgame', $data);
    
    }

    public function editGame($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // SANITIZE POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
    
                'Id' => $id,
                'Date' => trim($_POST['Date']),
                'ScoreHome' => trim($_POST['ScoreHome']),
                'ScoreVisitors' => trim($_POST['ScoreVisitors']),
                'Status' => trim($_POST['Status'])
                
                        ];
                        // validate data
                        // validate title
                        if(empty($data['Date'])){
                            $data['Date_err'] = 'Please enter a date';
                        }
                        if(empty($data['ScoreHome'])){
                            $data['ScoreHome_err'] = 'Please enter a home score';
                        }
                        if(empty($data['ScoreVisitors'])){
                            $data['ScoreVisitors_err'] = 'Please enter a visitors score';
                        }
                        if(empty($data['Status'])){
                            $data['Status_err'] = 'Please enter a status';
                        }
                     
    
                        // make sure there are no errors  
                        if(empty($data['Date_err']) && empty($data['ScoreHome_err']) && empty($data['ScoreVisitors_err']) && empty($data['Status_err'])){
                            // validated
                            if($this->gameModel->updateGame($data)){
                                
                                // hier is not iets niet juist...
                                flash('post_message', 'Game Updated');
                                redirect('game'); 
    
                            }else{
                                die('Something went wrong');
                            }
    
                        }else {
                            // load the view with errors
                            $this->view('competition/game/editgame', $data);
                        }
    
        } else{
    
            // get the existing player from the model
            $game = $this->gameModel->getGameById($id);
    
    
    
            // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
            // if($post->user_id != $_SESSION['user_id']){
            //     redirect('posts');
            // }
    
    
            // hier gaan we de waardes meegeven aan onze view door de variable $data op te vullen
            $data = [
        'Date' =>$game->Date,
        'Id' =>$game->Id,
        'Status' =>$game->Status,
        'ScoreHome' => $game->ScoreHome,
        'ScoreVisitors' =>$game->ScoreVisitors               
            ];
    
            // we nemen dus de id etc..  en geven die mee via $data aan de edit view
            // dit wordt dan samen aan de gebruiker getoond via de browser door de controler.
            $this->view('competition/game/editgame', $data);
        }
    }


    public function deleteGame($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // get the existing player from the model
            $game = $this->gameModel->getGameById($id);
            // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
            // if($player->user_id != $_SESSION['user_id']){
            //    redirect('footballs');
            // }
            if($this->gameModel->deleteGame($id)){
                flash('post_message', 'Game Removed');
                redirect('posts');
            }else {
                die('Something went wrong');
            }
        }else {
            redirect('posts');
        }
    
    }



    }