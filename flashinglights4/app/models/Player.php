<?php
class Player {
    private $db;

    public function  __construct(){
        $this->db = new Database;
    }


    // add player
    public function insertingone($data){
        $this->db->query('INSERT INTO Player (FirstName, LastName, Email, Address1, Address2, PostalCode, City, Country, Phone, Birthday ) VALUES(:FirstName, :LastName, :Email, :Address1, :Address2, :PostalCode, :City, :Country, :Phone, :Birthday )');
        // Bind values
        $this->db->bind(':FirstName', $data['FirstName']);
        $this->db->bind(':LastName', $data['LastName']);
        $this->db->bind(':Email', $data['Email']);
        $this->db->bind(':Address1', $data['Address1']);
        $this->db->bind(':Address2', $data['Address2']);
        $this->db->bind(':PostalCode', $data['PostalCode']);
        $this->db->bind(':City', $data['City']);
        $this->db->bind(':Country', $data['Country']);
        $this->db->bind(':Phone', $data['Phone']);
        $this->db->bind(':Birthday', $data['Birthday']);

    
    
    // Execute
    if($this->db->execute()){
    return true;
    } else {
        return false;
    }
    
}



//laatst toegevoegd.. test om alle players te tonen.
public function getPlayers(){
    $this->db->query('SELECT * FROM Player');

    $results = $this->db->resultSet();

    return $results;

}

public function getPlayerById($id){ // made id = :id into ID = :ID to test.. 
    $this->db->query('SELECT * FROM Player WHERE Id = :Id');
    $this->db->bind(':Id', $id);

    $row = $this->db->single();

    return $row;


}








public function updatePlayer($data){
        
    $this->db->query('UPDATE Player SET FirstName = :FirstName, LastName = :LastName, Email = :Email, Address1 = :Address1, Address2 = :Address2, PostalCode = :PostalCode, City = :City, Country = :Country, Phone = :Phone, Birthday = :Birthday WHERE Id = :Id');
    // Bind values
    $this->db->bind(':Id', $data['Id']);
    $this->db->bind(':FirstName', $data['FirstName']);
    $this->db->bind(':LastName', $data['LastName']);
    $this->db->bind(':Email', $data['Email']);
    $this->db->bind(':Address1', $data['Address1']);
    $this->db->bind(':Address2', $data['Address2']);
    $this->db->bind(':PostalCode', $data['PostalCode']);
    $this->db->bind(':City', $data['City']);
    $this->db->bind(':Country', $data['Country']);
    $this->db->bind(':Phone', $data['Phone']);
    $this->db->bind(':Birthday', $data['Birthday']);


// Execute
if($this->db->execute()){
return true;
} else {
    return false;
}
}


public function deletePlayer($id){
            
        
    $this->db->query('DELETE FROM Player WHERE Id = :Id');
    // Bind values
    $this->db->bind(':Id', $id);

    // Execute
    if($this->db->execute()){
return true;
} else {
    return false;
}
    
}







}
    


