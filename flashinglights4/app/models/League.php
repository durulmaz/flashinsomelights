<?php
class League {
    private $db;

    public function  __construct(){
        $this->db = new Database;
    }

    public function getLeagues(){
        $this->db->query('SELECT * FROM Liga');
    
        $results = $this->db->resultSet();
    
        return $results;
    
    }




    public function insertingone($data){
        $this->db->query('INSERT INTO Liga (Name, Year, IsInPlanning ) VALUES(:Name, :Year, :IsInPlanning)');
        // Bind values
        $this->db->bind(':Name', $data['Name']);
        $this->db->bind(':Year', $data['Year']);
        $this->db->bind(':IsInPlanning', $data['IsInPlanning']);

    // Execute
    if($this->db->execute()){
    return true;
    } else {
        return false;
    }
    
}



public function getLeagueById($id){ 
    $this->db->query('SELECT * FROM Liga WHERE Id = :Id');
    $this->db->bind(':Id', $id);

    $row = $this->db->single();

    return $row;


}




public function updateLeague($data){
        
    $this->db->query('UPDATE Liga SET Name = :Name, Year = :Year, IsInPlanning = :IsInPlanning WHERE Id = :Id');
    // Bind values
    $this->db->bind(':Id', $data['Id']);
    $this->db->bind(':Name', $data['Name']);
    $this->db->bind(':Year', $data['Year']);
    $this->db->bind(':IsInPlanning', $data['IsInPlanning']);

// Execute
if($this->db->execute()){
return true;
} else {
    return false;
}
}



public function deleteLeague($id){
            
        
    $this->db->query('DELETE FROM Liga WHERE Id = :Id');
    // Bind values
    $this->db->bind(':Id', $id);

    // Execute
    if($this->db->execute()){
return true;
} else {
    return false;
}
    
}







}