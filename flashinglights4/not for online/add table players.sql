use Cursist2;

-- Tania gebruikt Camelcase notatie voor entiteiten


CREATE TABLE players (
	
	firstname VARCHAR(30) NOT NULL,
	lastname VARCHAR(30) NOT NULL,
	email VARCHAR(50) NOT NULL,
    address1 varchar(50) NOT NULL,
    address2 varchar(50) NOT NULL,
    postalcode varchar(50) NOT NULL,
    city varchar(50) NOT NULL,
    country varchar(50) NOT NULL,
    phone INT(16) NOT NULL,
    birthday date NOT NULL,
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
    teamid INT(11) UNSIGNED auto_increment,
);